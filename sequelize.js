const Sequelize = require("sequelize");
const sequelize = process.env.DATABASE_URL
  ? new Sequelize(process.env.DATABASE_URL, {
      dialect: "postgres",
      protocol: "postgres",
      port: 5432,
      host: process.env.HOST_URL,
      logging: true
    })
  : new Sequelize({
      protocol: "postgres",
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      dialect: "postgres",
      host: "localhost",
      port: 5432,
      database: process.env.DB_NAME
    });

module.exports = sequelize;
