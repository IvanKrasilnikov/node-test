const sequelize = require("../sequelize");

const getReissues = (req, res) => {
  sequelize
    .query("SELECT * FROM book_reissue")
    .then(queryRes => {
      res.status(200).json(queryRes[0]);
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const getReissueById = (req, res) => {
  const id = parseInt(req.params.id);

  sequelize
    .query(`SELECT * FROM book_reissue WHERE book_reissue_id = ${id}`)
    .then(queryRes => {
      res.status(200).json(queryRes[0][0]);
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const getReissuesByBookId = (req, res) => {
  const id = parseInt(req.params.id);

  sequelize
    .query(`SELECT * FROM book_reissue WHERE book_id = ${id}`)
    .then(queryRes => {
      res.status(200).json(queryRes[0]);
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const createReissue = (req, res) => {
  const { reissue, reissueDate, bookId } = req.body;

  sequelize
    .query(
      `INSERT INTO book_reissue (book_reissue, book_reissue_date, book_id) VALUES ('${reissue}', '${reissueDate}', ${bookId})`
    )
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const updateReissue = (req, res) => {
  const id = parseInt(req.params.id);
  const { reissue, reissueDate, bookId } = req.body;

  sequelize
    .query(
      `UPDATE book_reissue SET book_reissue='${reissue}', book_reissue_date='${reissueDate}', book_id='${bookId}' WHERE book_reissue_id=${id}`
    )
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const deleteReissue = (req, res) => {
  const id = parseInt(req.params.id);

  sequelize
    .query(`DELETE FROM book_reissue WHERE book_reissue_id=${id}`)
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

module.exports = {
  getReissues,
  getReissueById,
  getReissuesByBookId,
  createReissue,
  updateReissue,
  deleteReissue
};
