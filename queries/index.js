const authors = require("./authors");
const booksWithAuthors = require("./booksWithAuthors");
const reissues = require("./reissues");

module.exports = {
  ...authors,
  ...booksWithAuthors,
  ...reissues
};
