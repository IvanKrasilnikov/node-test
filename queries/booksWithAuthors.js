const sequelize = require("../sequelize");

function concatAuthors(books) {
  return books.reduce((acc, el) => {
    const prevEl = acc[acc.length - 1];
    const authorEl = {
      author: el.author,
      author_date: el.author_date,
      author_id: el.author_id
    };
    const resultEl = {
      book_id: el.book_id,
      book: el.book,
      book_date: el.book_date,
      authors: [authorEl]
    };

    if (el.book_id === (prevEl && prevEl.book_id)) {
      resultEl.authors = resultEl.authors.concat(prevEl.authors);
      acc[acc.length - 1] = resultEl;
    } else {
      acc.push(resultEl);
    }

    return acc;
  }, []);
}

const getBooksWithAuthors = async (req, res) => {
  try {
    const books = await sequelize.query(`
      SELECT * FROM book_author
      INNER JOIN book ON book_author.book_id = book.book_id
      INNER JOIN author ON book_author.author_id = author.author_id
      ORDER BY book.book_id ASC
    `);

    res.status(200).json(concatAuthors(books[0]));
  } catch (err) {
    res.status(500).send(err);
  }
};

const getBookWithAuthorById = async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    const books = await sequelize.query(`
      SELECT * FROM book_author
      INNER JOIN book ON book_author.book_id = book.book_id
      INNER JOIN author ON book_author.author_id = author.author_id
      ORDER BY book.book_id ASC
    `);

    res
      .status(200)
      .json(concatAuthors(books[0]).find(book => book.book_id === id));
  } catch (err) {
    res.status(500).send(err);
  }
};

const createBookWithAuthor = async (req, res) => {
  try {
    const { book, bookDate, authorId } = req.body;

    const createdBookWithAuthor = await sequelize.query(
      `INSERT INTO book (book, book_date) VALUES ('${book}', '${bookDate}') RETURNING book_id`
    );

    if (authorId) {
      const bookId = createdBookWithAuthor[0][0].book_id;

      await sequelize.query(
        `INSERT INTO book_author (book_id, author_id) VALUES (${bookId}, ${authorId})`
      );
    }

    res.status(200).send("complete");
  } catch (err) {
    res.status(500).send(err);
  }
};

const updateBookWithAuthor = async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    const { book, bookDate, authorId } = req.body;

    await sequelize.query(
      `UPDATE book SET book='${book}', book_date='${bookDate}' WHERE book_id=${id}`
    );

    if (authorId) {
      await sequelize.query(
        `UPDATE book_author SET author_id='${authorId}' WHERE book_id=${id}`
      );
    }

    res.status(200).send("complete");
  } catch (err) {
    res.status(500).send(err);
  }
};

const deleteBookWithAuthor = async (req, res) => {
  try {
    const id = parseInt(req.params.id);

    await sequelize.query(`DELETE FROM book WHERE book_id=${id}`);

    res.status(200).send("complete");
  } catch (err) {
    res.status(500).send(err);
  }
};

module.exports = {
  getBooksWithAuthors,
  getBookWithAuthorById,
  createBookWithAuthor,
  updateBookWithAuthor,
  deleteBookWithAuthor
};
