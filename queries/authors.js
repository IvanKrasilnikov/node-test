const sequelize = require("../sequelize");

const getAuthors = (req, res) => {
  sequelize.models.authors
    .findAll()
    .then(queryRes => {
      res.status(200).json(queryRes);
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const getAuthorById = (req, res) => {
  const id = parseInt(req.params.id);

  sequelize.models.authors
    .findOne({
      where: { authorId: id }
    })
    .then(queryRes => {
      res.status(200).json(queryRes);
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const createAuthor = (req, res) => {
  const { author, authorDate } = req.body;

  sequelize.models.authors
    .upsert({ author, authorDate })
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const updateAuthor = (req, res) => {
  const id = parseInt(req.params.id);
  const { author, authorDate } = req.body;

  sequelize.models.authors
    .update({ author, authorDate }, {
      where: {
        authorId: id,
      },
    })
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

const deleteAuthor = (req, res) => {
  const id = parseInt(req.params.id);

  sequelize.models.authors
    .destroy({
      where: {
        authorId: id,
      },
    })
    //.query(`DELETE FROM author WHERE author_id=${id}`)
    .then(queryRes => {
      return res.status(200).send("complete");
    })
    .catch(error => {
      res.status(500).send("error");
    });
};

module.exports = {
  getAuthors,
  getAuthorById,
  createAuthor,
  updateAuthor,
  deleteAuthor
};
