# node-test

Приложение для работы с базой книг и авторов.
У одной книги может быть несколько авторов.
У книги может быть переиздание (reissue).

## db
Связь таблицы book с таблицей book_reissue выражает отношение one-to-many (у книги может быть много переизданий, переиздание может быть только для одной книги).
Связь таблицы book с таблицей author реализована через таблицу связанности book_author и выражает отношение many-to-many (у книги может быть много авторов, у автора может быть много книг).