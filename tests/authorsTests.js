const request = require("supertest");
const app = require("../server");

describe("My API authors tests", function() {
  const basicAuthorizationToken = `${process.env.OKTA_CLIENT_ID}:${
    process.env.OKTA_CLIENT_SECRET
  }`.toString("base64");
  let sessionToken = null;

  before(function(done) {
    request(process.env.OKTA_ORG_URL)
      .post("/api/v1/authn")
      .set({
        Accept: "application/json",
        "Content-Type": "application/json"
      })
      .send({
        username: "lol@lol.lol",
        password: "Porsche911",
        options: {
          multiOptionalFactorEnroll: false,
          warnBeforePasswordExpired: false
        }
      })
      .end(function(err, res) {
        sessionToken = res.body.sessionToken;
        done();
      });
  });

  before(function(done) {
    request(process.env.OKTA_ORG_URL)
      .post("/oauth2/default/v1/token")
      .set({
        Accept: "application/json",
        Authorization: `Basic ${basicAuthorizationToken}`,
        "content-type": "application/x-www-form-urlencoded"
      })
      .send({
        grant_type: "authorization_code",
        redirect_uri: `${process.env.HOST_URL}/authorization-code/callback`,
        code: sessionToken,
      })
      .end(function(err, res) {
        //console.log(res);
        done();
      });
  });

  it("respond with json containing a list of all authors", function(done) {
    request
      .agent(app)
      .set("Authorization", `Bearer ${sessionToken}`)
      .get("/authors")
      //.set('Accept', 'application/json')
      //.expect('Content-Type', /json/)
      //.expect(200, done)
      .end(function(err, res) {
        done();
      });
  });
});
