const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const session = require("express-session");

require("dotenv").load();
require("custom-env").env(true);
require("./models");

const oidc = require("./oidc");
const queries = require("./queries");

app.use(
  session({
    secret: process.env.APP_SECRET,
    resave: true,
    saveUninitialized: false
  })
);

app.use(oidc.router);

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.get("/", oidc.ensureAuthenticated(), (req, res) => {
  res.send("Node.js, Express, and Postgres API");
});

app.get("/logout", (req, res) => {
  if (req.userContext) {
    const idToken = req.userContext.tokens.id_token;
    const to = encodeURI(process.env.HOST_URL);
    const params = `id_token_hint=${idToken}&post_logout_redirect_uri=${to}/logout`;

    req.logout();
    res.redirect(
      `${process.env.OKTA_ORG_URL}/oauth2/default/v1/logout?${params}`
    );
  } else {
    res.redirect("/");
  }
});

app.get("/reissues", oidc.ensureAuthenticated(), queries.getReissues);
app.get("/reissues/:id", oidc.ensureAuthenticated(), queries.getReissueById);
app.post("/reissues", oidc.ensureAuthenticated(), queries.createReissue);
app.put("/reissues/:id", oidc.ensureAuthenticated(), queries.updateReissue);
app.delete("/reissues/:id", oidc.ensureAuthenticated(), queries.deleteReissue);

app.get(
  "/booksReissues/:id",
  oidc.ensureAuthenticated(),
  queries.getReissuesByBookId
);

app.get("/authors", oidc.ensureAuthenticated(), queries.getAuthors);
app.get("/authors/:id", oidc.ensureAuthenticated(), queries.getAuthorById);
app.post("/authors", oidc.ensureAuthenticated(), queries.createAuthor);
app.put("/authors/:id", oidc.ensureAuthenticated(), queries.updateAuthor);
app.delete("/authors/:id", oidc.ensureAuthenticated(), queries.deleteAuthor);

app.get(
  "/booksAuthors",
  oidc.ensureAuthenticated(),
  queries.getBooksWithAuthors
);
app.get(
  "/booksAuthors/:id",
  oidc.ensureAuthenticated(),
  queries.getBookWithAuthorById
);
app.post(
  "/booksAuthors",
  oidc.ensureAuthenticated(),
  queries.createBookWithAuthor
);
app.put(
  "/booksAuthors/:id",
  oidc.ensureAuthenticated(),
  queries.updateBookWithAuthor
);
app.delete(
  "/booksAuthors/:id",
  oidc.ensureAuthenticated(),
  queries.deleteBookWithAuthor
);

oidc.on("ready", () => {
  app.listen(process.env.PORT || 3000);
});

module.exports = app;
