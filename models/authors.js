const Sequelize = require("sequelize");
const sequelize = require("../sequelize");

sequelize.define(
  "authors",
  {
    authorId: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      field: "author_id"
    },
    author: {
      type: Sequelize.STRING,
      unique: true,
      field: "author"
    },
    authorDate: {
      type: Sequelize.DATE,
      validate: {
        isDate: true
      },
      field: "author_date"
    }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
    tableName: "author"
  }
);