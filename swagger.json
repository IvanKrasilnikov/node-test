{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "node-test"
  },
  "host": "pure-lowlands-10120.herokuapp.com",
  "schemes": [
    "https"
  ],
  "basePath": "/",
  "definitions": {
    "author": {
      "required": [
        "author",
        "author_date"
      ],
      "type": "object",
      "properties": {
        "author_id": {
          "type": "integer",
          "format": "int64"
        },
        "author": {
          "type": "string"
        },
        "author_date": {
          "type": "string",
          "format": "date"
        }
      }
    },
    "book_reissue": {
      "required": [
        "book_id",
        "book_reissue",
        "book_date"
      ],
      "type": "object",
      "properties": {
        "book_reissue_id": {
          "type": "integer",
          "format": "int64"
        },
        "book_id": {
          "type": "integer",
          "format": "int64"
        },
        "book_reissue": {
          "type": "string"
        },
        "book_date": {
          "type": "string",
          "format": "date"
        }
      }
    },
    "book_author": {
      "required": [
        "book_id",
        "author_id"
      ],
      "type": "object",
      "properties": {
        "book_id": {
          "type": "integer",
          "format": "int64"
        },
        "author_id": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "requestReissue": {
      "type": "object",
      "required": [
        "reissue",
        "reissueDate",
        "bookId"
      ],
      "properties": {
        "reissue": {
          "type": "string"
        },
        "reissueDate": {
          "type": "string",
          "format": "date"
        },
        "bookId": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "requestAuthor": {
      "type": "object",
      "required": [
        "author",
        "authorDate"
      ],
      "properties": {
        "author": {
          "type": "string"
        },
        "authorDate": {
          "type": "string",
          "format": "date"
        }
      }
    },
    "responseBookAuthors": {
      "type": "object",
      "properties": {
        "authors": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "author_id": {
                "type": "integer",
                "format": "int64"
              },
              "author": {
                "type": "string"
              },
              "author_date": {
                "type": "string",
                "format": "date"
              }
            }
          }
        },
        "book_id": {
          "type": "integer",
          "format": "int64"
        },
        "book": {
          "type": "string"
        },
        "book_date": {
          "type": "string",
          "format": "date"
        }
      }
    },
    "requestBookAuthor": {
      "type": "object",
      "required": [
        "book",
        "bookDate"
      ],
      "properties": {
        "book": {
          "type": "string"
        },
        "bookDate": {
          "type": "string",
          "format": "date"
        },
        "authorId": {
          "type": "integer",
          "format": "int64"
        }
      }
    }
  },
  "responses": {
    "authors": {
      "description": "A list of authors",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/author"
        }
      }
    },
    "reissues": {
      "description": "A list of reissues",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/book_reissue"
        }
      }
    },
    "bookWithAuthors": {
      "description": "A book with authors",
      "schema": {
        "$ref": "#/definitions/responseBookAuthors"
      }
    },
    "booksWithAuthors": {
      "description": "A list of books with authors",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/responseBookAuthors"
        }
      }
    }
  },
  "paths": {
    "/booksAuthors": {
      "get": {
        "summary": "Get all original books with authors",
        "description": "Only original books with authors, without reissues",
        "responses": {
          "200": {
            "$ref": "#/responses/booksWithAuthors"
          }
        }
      },
      "post": {
        "summary": "Create original book and associate with author",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestBookAuthor"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/booksAuthors/{id}": {
      "get": {
        "summary": "Get original book with authors by ID",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of book",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/bookWithAuthors"
          }
        }
      },
      "put": {
        "summary": "Update original book",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of book",
            "required": true,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestBookAuthor"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      },
      "delete": {
        "summary": "Delete original book",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of book",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/booksReissues/{id}": {
      "get": {
        "summary": "Get reissue by book ID",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of book",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "$ref": "#/definitions/book_reissue"
            }
          }
        }
      }
    },
    "/reissues": {
      "get": {
        "summary": "Get all reissues of books",
        "responses": {
          "200": {
            "$ref": "#/responses/reissues"
          }
        }
      },
      "post": {
        "summary": "Create reissue of book",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestReissue"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/reissues/{id}": {
      "get": {
        "summary": "Get reissue by ID",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of reissue",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "$ref": "#/definitions/book_reissue"
            }
          }
        }
      },
      "put": {
        "summary": "Update reissue",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of reissue",
            "required": true,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestReissue"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      },
      "delete": {
        "summary": "Delete reissue",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of reissue",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/authors": {
      "get": {
        "summary": "Get all authors",
        "responses": {
          "200": {
            "$ref": "#/responses/authors"
          }
        }
      },
      "post": {
        "summary": "Create author",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestAuthor"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/authors/{id}": {
      "get": {
        "summary": "Get author by ID",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of author",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "$ref": "#/definitions/author"
            }
          }
        }
      },
      "put": {
        "summary": "Update author",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of author",
            "required": true,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/requestAuthor"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      },
      "delete": {
        "summary": "Delete author",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of author",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    }
  }
}